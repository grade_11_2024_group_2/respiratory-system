[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [terminologies](terminologies.md)
# Welcome to our Respiratory Project!
![](images/home.jpg)



#### About Us:
 Welcome to Lung Logic Innovations! We're a team of four students driven by curiosity and a passion for understanding the respiratory system better. Our project focuses on creating a working model of the respiratory system to explore its complexities and implications for the environment and human health. Meet our team: Sumjay, Kinley Sonam, Rinzin Wangmo, and Tenzin Rabgay Zangmo. Together, we're blending technology and nature-inspired innovation to unravel the mysteries of breathing.

 ## Inspiration  
![](images/ins.jpg)

 Our idea to create a respiratory system with a breathing monitor was sparked by our struggles in understanding how breathing works when we were in 10th grade. We remember how confusing it was, and we want to make it easier for others to understand. By using technology and our own experiences, we hope to simplify the concept and help others see how amazing the respiratory system really is.

 ## Acknowledgement 
 We want to extend our heartfelt thanks to all the helpers who were always there for us, assisting in the development of our project. Your support has been invaluable, and we could not have reached this point without your dedication and encouragement.

 We also want to thank our group members for their hard work, collaboration, and unwavering commitment. We are glad to have worked as a team, where everyone was equally engaged and able to learn a lot from each other. Each member's unique skills and perspectives have been crucial in shaping our project, and it has been a pleasure working together.

In particular, we would like to express our deepest gratitude to our technology teacher, Mr. Anith. His guidance through various concepts has been instrumental in building our final project. Mr. Anith’s expertise, patience, and willingness to share his knowledge have significantly contributed to our learning and success.

Thank you to everyone who played a part in our journey. Your contributions, big and small, have made a lasting impact, and we are truly grateful for your help and support.


[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)
 