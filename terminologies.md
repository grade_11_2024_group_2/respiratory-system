[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [terminologies](terminologies.md)
1. Arduino UNO or compatible board: A microcontroller board for prototyping digital devices.
![](images/arduino.jpg)
2. USB cable: Used to connect the Arduino board to a computer for programming and power.
![](images/usb.jpg)
3. Full-sized breadboard: A solderless board for building electronic circuits.
![](images/bre.jpg)


4. Solid jumper wire kit: Wires used to connect components on the breadboard.
![](images/jump.jpg)


5. Male-male flexible jumper wires: Flexible wires for connecting components.

6. Alligator clip leads: Clips used for temporary connections.
![](images/ali.jpg)


7. 10kΩ potentiometers: Adjustable resistors for voltage control.
### potentiometers
![](images/poten.jpg)
8. 1/4W 220Ω resistors: Resistors limiting electric current.

9. 1/4W 4.7kΩ resistors: Resistors with higher resistance.
![](images/resis.jpg)




10. Optional: 1/4W resistor kit: Kit with various resistor values.

11. Diffused 5mm green LEDs: Green light-emitting diodes.

12. Diffused 5mm blue LEDs: Blue light-emitting diodes.

13. Optional: LED kit: Kit with various LED colors.

14. Conductive rubber cord: Flexible cord for conducting electricity.
  ### Conductive rubber cord
![](images/con.jpg)






[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)
